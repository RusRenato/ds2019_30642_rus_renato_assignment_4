package com.example.consumingsoap.controller;

import com.example.consumingsoap.client.MedicationClient;
import com.example.consumingwebservice.wsdl.Medication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MedicationController {

    @Autowired
    MedicationClient medicationClient;

    @CrossOrigin
    @GetMapping("medications/{id}")
    public List<Medication> get(@PathVariable int id) {
        return medicationClient.getMedicationsResponse(id).getMedicationsList().getMedication();
    }

    @CrossOrigin
    @PutMapping("recommendation/{id}")
    public String letRecommendation(@PathVariable int id, @RequestBody String recommendation) {
        medicationClient.letRecommendationRequest(id,recommendation);
        return "Succes";
    }

}
