package com.example.consumingsoap.controller;

import com.example.consumingsoap.client.ActivityClient;
import com.example.consumingwebservice.wsdl.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ActivityController {

    @Autowired
    private ActivityClient activityClient;

    @CrossOrigin
    @GetMapping("activity/{id}/day/{day}")
    public List<Activity> getActivitiesOfUserWithId(@PathVariable int id, @PathVariable String day) {
        return activityClient.getActivity(id).getActivity().getActivity().stream().filter(activityDTO -> activityDTO.getEnd().substring(8, 10).equals(day)).collect(Collectors.toList());
    }

    @CrossOrigin
    @PutMapping("activity/{id}/isNormal/{normal}")
    public String markIsNormal(@PathVariable int id, @PathVariable boolean normal) {
        activityClient.markActivity(id, normal);
        return "Succes";
    }


}
