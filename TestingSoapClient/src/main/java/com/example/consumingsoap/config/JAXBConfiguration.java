package com.example.consumingsoap.config;

import com.example.consumingsoap.client.ActivityClient;
import com.example.consumingsoap.client.MedicationClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class JAXBConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("com.example.consumingwebservice.wsdl");
        return marshaller;
    }

    @Bean
    public ActivityClient activityClient(Jaxb2Marshaller marshaller) {
        ActivityClient client = new ActivityClient();
        client.setDefaultUri("http://localhost:8081/ws/activity");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public MedicationClient medicationClient(Jaxb2Marshaller marshaller) {
        MedicationClient client = new MedicationClient();
        client.setDefaultUri("http://localhost:8081/ws/medication");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
