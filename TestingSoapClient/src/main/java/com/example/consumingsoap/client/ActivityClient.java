package com.example.consumingsoap.client;

import com.example.consumingwebservice.wsdl.GetActivityRequest;
import com.example.consumingwebservice.wsdl.GetActivityResponse;
import com.example.consumingwebservice.wsdl.MarkActivityRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;


public class ActivityClient extends WebServiceGatewaySupport {
    private static final Logger Log = LoggerFactory.getLogger(ActivityClient.class);

    public GetActivityResponse getActivity(int id) {
        GetActivityRequest request = new GetActivityRequest();
        request.setId(id);

        Log.info("Requesting activity for user with id {}", id);;

        GetActivityResponse response = (GetActivityResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8081/ws/activities",request,
                new SoapActionCallback("getActivityRequest/GetActivityRequest"));
        return response;
    }

    public void markActivity(int id, boolean normal) {
        MarkActivityRequest request = new MarkActivityRequest();
        request.setId(id);
        request.setNormal(normal);
        getWebServiceTemplate().marshalSendAndReceive("http://localhost:8081/ws/activities",request,
                new SoapActionCallback("markActivityRequest/MarkActivityRequest"));
    }

}
