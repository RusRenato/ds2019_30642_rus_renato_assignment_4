package com.example.consumingsoap.client;

import com.example.consumingwebservice.wsdl.GetMedicationsRequest;
import com.example.consumingwebservice.wsdl.GetMedicationsResponse;
import com.example.consumingwebservice.wsdl.LetRecommendationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class MedicationClient extends WebServiceGatewaySupport {
    private static final Logger Log = LoggerFactory.getLogger(MedicationClient.class);

    public GetMedicationsResponse getMedicationsResponse(int id) {
        GetMedicationsRequest request = new GetMedicationsRequest();
        request.setUserId(id);

        Log.info("Requesting activity for user with id {}", id);

        GetMedicationsResponse response = (GetMedicationsResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8081/ws/medications", request,
                new SoapActionCallback("getMedicationsRequest/getMedicationsRequest"));
        return response;
    }

    public void letRecommendationRequest(int id, String recommendation) {
        LetRecommendationRequest request = new LetRecommendationRequest();
        request.setUserId(id);
        request.setRecommendation(recommendation);

        Log.info("Letting recommendation {} for user with id {} ",recommendation, id);

        getWebServiceTemplate().marshalSendAndReceive("http://localhost:8081/ws/medications", request,
                new SoapActionCallback("letRecommendationRequest/LetRecommendationRequest"));
    }
}
