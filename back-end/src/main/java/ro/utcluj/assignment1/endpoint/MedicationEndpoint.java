package ro.utcluj.assignment1.endpoint;

import medicationnamespace.GetMedicationsRequest;
import medicationnamespace.GetMedicationsResponse;
import medicationnamespace.LetRecommendationRequest;
import medicationnamespace.MedicationsList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ro.utcluj.assignment1.dto.Mapper;
import ro.utcluj.assignment1.model.MedicationIntervals;
import ro.utcluj.assignment1.model.MedicationPlan;
import ro.utcluj.assignment1.service.UserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Endpoint
public class MedicationEndpoint {
    private static final String NAMESPACE_URI = "medicationNameSpace";

    @Autowired
    UserService userService;
    @Autowired
    Mapper mapper;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMedicationsRequest")
    @ResponsePayload
    public GetMedicationsResponse getActivity(@RequestPayload GetMedicationsRequest request) {
        GetMedicationsResponse response = new GetMedicationsResponse();
        MedicationsList medicationsList = new MedicationsList();

        List<Set<MedicationIntervals>> medicationIntervals = userService.getUserMedicationPlans(request.getUserId()).stream().
                map(MedicationPlan::getMedications).
                collect(Collectors.toList());
        for(Set<MedicationIntervals> medicationIntervalsSet : medicationIntervals) {
            for (MedicationIntervals medicationIntervals1 : medicationIntervalsSet) {
                medicationsList.getMedication().add(mapper.map(medicationIntervals1));
            }
        }

        response.setMedicationsList(medicationsList);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "letRecommendationRequest")
    @ResponsePayload
    public void letRecommendation(@RequestPayload LetRecommendationRequest request) {
        userService.addRecommendation(request.getUserId(), request.getRecommendation());
    }

}
