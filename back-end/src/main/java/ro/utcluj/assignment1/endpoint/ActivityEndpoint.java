package ro.utcluj.assignment1.endpoint;

import activitynamespace.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ro.utcluj.assignment1.service.ActivityService;

import java.util.stream.Collectors;

@Endpoint
public class ActivityEndpoint {
    private static final String NAMESPACE_URI = "activityNameSpace";

    @Autowired
    ActivityService activityService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivityRequest")
    @ResponsePayload
    public GetActivityResponse getActivity(@RequestPayload GetActivityRequest request) {
        GetActivityResponse response = new GetActivityResponse();

        ActivitiesList activitiesList = new ActivitiesList();
        activityService.getActivitesOfAUser(request.getId()).stream().map(this::convert).collect(Collectors.toList()).forEach(activity -> activitiesList.getActivity().add(activity));
        response.setActivity(activitiesList);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "markActivityRequest")
    @ResponsePayload
    public void markActivity(@RequestPayload MarkActivityRequest request) {
        activityService.setIsNormal(request.getId(),request.isNormal());
    }

    private Activity convert(ro.utcluj.assignment1.model.Activity activities) {
        Activity SOAPActivity = new Activity();
        SOAPActivity.setActivity(activities.getActivity());
        SOAPActivity.setStart(activities.getStart());
        SOAPActivity.setEnd(activities.getEnd());
        SOAPActivity.setDoneRight(activities.isDoneRight());
        SOAPActivity.setIsNormal(activities.isNormal());
        SOAPActivity.setId(activities.getId());

        return SOAPActivity;
    }
}
