package ro.utcluj.assignment1.service;

import ro.utcluj.assignment1.model.Activity;

import java.util.List;

public interface ActivityService {
    List<Activity> getAllActivities();

    void setDoneRight(Activity activity,boolean d);

    void update();

    List<Activity> getActivitesOfAUser(int userId);

    void setIsNormal(int id, boolean normal);
}
