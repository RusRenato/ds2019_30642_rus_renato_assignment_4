package ro.utcluj.assignment1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcluj.assignment1.model.Activity;
import ro.utcluj.assignment1.repository.ActivityRepository;
import ro.utcluj.assignment1.service.ActivityService;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public List<Activity> getAllActivities() {
        return activityRepository.findAll();
    }

    @Override
    public void setDoneRight(Activity activity, boolean d) {
        activity.setDoneRight(d);
        activityRepository.save(activity);
    }

    @Override
    public void update() {
        for(Activity activity : activityRepository.findAll()){
            activity.setDoneRight(true);
            activityRepository.save(activity);
        }
    }

    @Override
    public List<Activity> getActivitesOfAUser(int userId) {
        return activityRepository.getActivitiesByUserId(userId);
    }

    @Override
    public void setIsNormal(int id, boolean normal) {
        Activity activity = activityRepository.getActivityById(id);
        activity.setNormal(normal);
        activityRepository.save(activity);
    }


}
