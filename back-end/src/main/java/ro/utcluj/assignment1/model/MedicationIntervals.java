package ro.utcluj.assignment1.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;

@Entity
public class MedicationIntervals implements Serializable {
    private static final long serialVersionUID = 3132943596280889097L;
    @Id
    @GeneratedValue
    private int id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "medication_id")
    private Medication medication;
    private LocalTime intakeStartInterval;
    private LocalTime intakeEndInterval;
    private boolean taken;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public LocalTime getIntakeStartInterval() {
        return intakeStartInterval;
    }

    public void setIntakeStartInterval(LocalTime intakeStartInterval) {
        this.intakeStartInterval = intakeStartInterval;
    }

    public LocalTime getIntakeEndInterval() {
        return intakeEndInterval;
    }

    public void setIntakeEndInterval(LocalTime intakeEndInterval) {
        this.intakeEndInterval = intakeEndInterval;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "MedicationIntervals{" +
                "id=" + id +
                ", medication=" + medication +
                ", intakeStartInterval=" + intakeStartInterval +
                ", intakeEndInterval=" + intakeEndInterval +
                ", taken=" + taken +
                '}';
    }
}
