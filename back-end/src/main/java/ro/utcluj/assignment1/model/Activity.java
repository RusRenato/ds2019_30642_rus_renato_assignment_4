package ro.utcluj.assignment1.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "activity")
public class Activity implements Serializable {
    private static final long serialVersionUID = -8190721996146390827L;
    @GeneratedValue
    @Id
    private int id;
    private String activity;
    private String start;
    private String end;
    private boolean doneRight;
    private boolean isNormal;
    private int userId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean isDoneRight() {
        return doneRight;
    }

    public void setDoneRight(boolean doneRight) {
        this.doneRight = doneRight;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isNormal() {
        return isNormal;
    }

    public void setNormal(boolean normal) {
        isNormal = normal;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", activity='" + activity + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", doneRight=" + doneRight +
                ", isNormal=" + isNormal +
                ", userId=" + userId +
                '}';
    }
}
