package ro.utcluj.assignment1.dto;

public class MedicationIntervalsSOAP {
    private String medicationName;
    private String intakeStartInterval;
    private String intakeEndInterval;
    private boolean taken;

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getIntakeStartInterval() {
        return intakeStartInterval;
    }

    public void setIntakeStartInterval(String intakeStartInterval) {
        this.intakeStartInterval = intakeStartInterval;
    }

    public String getIntakeEndInterval() {
        return intakeEndInterval;
    }

    public void setIntakeEndInterval(String intakeEndInterval) {
        this.intakeEndInterval = intakeEndInterval;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "MedicationIntervalsDTOforSOAP{" +
                "medicationName='" + medicationName + '\'' +
                ", intakeStartInterval='" + intakeStartInterval + '\'' +
                ", intakeEndInterval='" + intakeEndInterval + '\'' +
                ", taken=" + taken +
                '}';
    }
}
