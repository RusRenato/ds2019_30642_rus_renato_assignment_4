package ro.utcluj.assignment1.dto;

public class ActivityDTO {
    private String activity;
    private String start;
    private String end;
    private boolean doneRight;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean isDoneRight() {
        return doneRight;
    }

    public void setDoneRight(boolean doneRight) {
        this.doneRight = doneRight;
    }

    @Override
    public String toString() {
        return "ActivityDTO{" +
                "activity='" + activity + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", doneRight=" + doneRight +
                '}';
    }
}
