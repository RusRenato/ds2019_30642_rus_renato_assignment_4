package ro.utcluj.assignment1.repository;

import ro.utcluj.assignment1.model.Activity;

import java.util.List;

public interface ActivityRepository extends BaseRepository<Activity> {
    @Override
    List<Activity> findAll();

    List<Activity> getActivitiesByUserId(int id);

    Activity getActivityById(int id);
}
