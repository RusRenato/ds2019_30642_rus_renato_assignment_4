package ro.utcluj.assignment1.config;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    @Bean(name = "activities")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema activitiesSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("ActivitiesPort");
        wsdl11Definition.setLocationUri("/activity");
        wsdl11Definition.setTargetNamespace("activityNameSpace");
        wsdl11Definition.setSchema(activitiesSchema);
        return wsdl11Definition;
    }

    @Bean(name = "medications")
    public DefaultWsdl11Definition defaultWsdl11DefinitionfroMedication(XsdSchema medicationsSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MedicationsPort");
        wsdl11Definition.setLocationUri("/medication");
        wsdl11Definition.setTargetNamespace("medicationNameSpace");
        wsdl11Definition.setSchema(medicationsSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema activitiesSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/activity.xsd"));
    }

    @Bean
    public XsdSchema medicationsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/medication.xsd"));
    }
}
