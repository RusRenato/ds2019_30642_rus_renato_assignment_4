package ro.utcluj.assignment1.producer;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ro.utcluj.assignment1.model.Activity;
import ro.utcluj.assignment1.service.ActivityService;

@Component
public class Producer {

    @Autowired
    private FileReader fileReader;

    @Autowired
    private ActivityService activityService;

    @Autowired
    @Qualifier("myBean")
    private AmqpTemplate amqpTemplate;

    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingKey}")
    private String routingKey;


    public void produceMsg() {
        for (Activity activity : activityService.getAllActivities()) {
            amqpTemplate.convertAndSend(exchange, routingKey, activity);
            System.out.println("Send mesg = " + activity);
        }
    }


}
