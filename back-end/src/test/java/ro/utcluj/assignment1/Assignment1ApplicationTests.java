package ro.utcluj.assignment1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ro.utcluj.assignment1.dto.Mapper;
import ro.utcluj.assignment1.model.Activity;
import ro.utcluj.assignment1.model.MedicationIntervals;
import ro.utcluj.assignment1.model.MedicationPlan;
import ro.utcluj.assignment1.service.ActivityService;
import ro.utcluj.assignment1.service.UserService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Assignment1ApplicationTests {
    @Autowired
    UserService userService;
    @Autowired
    ActivityService activityService;
    @Autowired
    Mapper mapper;

    @Test
    public void test2() {
        userService.getPatientById(5).getActivities().forEach(System.out::println);
    }

    @Test
    public void test3() {
        activityService.update();
    }

    @Test
    public void test4() {
        System.out.println(activityService.getActivitesOfAUser(5));
    }

    @Test
    public void test5() {
        List<Set<MedicationIntervals>> medicationIntervals = userService.getUserMedicationPlans(5).stream().
                                                            map(MedicationPlan::getMedications).
                                                            collect(Collectors.toList());
        for(Set<MedicationIntervals> medicationIntervalsSet : medicationIntervals) {
            for (MedicationIntervals medicationIntervals1 : medicationIntervalsSet) {
                System.out.println(mapper.map(medicationIntervals1));
            }
        }
    }

    @Test
    public void test6() {
        userService.addRecommendation(5,"Sa se spele lol");
        System.out.println(userService.getPatientById(5).getRecommendations());
    }

}
